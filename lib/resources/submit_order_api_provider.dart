import 'dart:convert';

import 'package:http/http.dart' show Client;


class SubmitOrderForm{
  Future<Map> submitOrder(var body) async{
    Client _client = Client();

    try{
      final _response = await _client.post(
          "http://hoitead012/ad1gateapi/api/Submit/SubmitOrderAd1gate",
          headers: {"Content-Type":"application/json"},
          body: body
      );
      print(_response.statusCode);
      var _listDataSuksesSubmit = [];
      if(_response.statusCode == 200){
        final _data = jsonDecode(_response.body);
        for(var i=0; i< _data['Data'][i].length; i++){
          _listDataSuksesSubmit.add(_data['Data'][i]);
        }
        var _result = {"status":true,"listDataSuksesSubmit":_listDataSuksesSubmit};
        return _result;
      }
      else{
        var _result = {"status":false,"message":"Failed get data"};
        return _result;
      }
    }
    catch(e){
      var _result = {"status":false,"message":"Failed get data"};
      return _result;
    }
  }
}