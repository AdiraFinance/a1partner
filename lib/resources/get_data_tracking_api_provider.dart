import 'dart:convert';

import 'package:adira_finance/constan/url.dart';
import 'package:adira_finance/model/submit_order_model.dart';
import 'package:http/http.dart' show Client;
import 'package:shared_preferences/shared_preferences.dart';

class GetDataTrackingApiProvider{
  Client _client = Client();
  Future<Map> getBranchByDLC() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _userDLC = _preferences.getString("userDLC");
    try{
      final _response = await _client.get("${BaseUrl.url}Tracking/GetBranchByDLC?dlc=$_userDLC");
      final _data = jsonDecode(_response.body);
      print(_data.toString());
      if(_data['Status'] == 0){
        if(_data['Data'].length > 1){
          var _listBranch = [];
          for(int i=0; i<_data["Data"].length; i++){
            _listBranch.add(_data['Data'][i]);
          }
          var _result = {"status": true, "listBranch":_listBranch};
          print("cek result $_result");
          return _result;
        }
        else{
          print("data kosong ${_data['Data'].length}");
          var _result = {"status":false, "message":"0"};
          return _result;
        }
      }
      else{
        var _result = {"status": false, "message":_data['Message']};
        return _result;
      }
    }
    catch(e){
      var _result = {"status": false,"message":e.toString()};
      return _result;
    }
  }

  Future<Map> getStatusOrder(String idTracking) async{
    Client _client = Client();
    try{
      final _response = await _client.get("${BaseUrl.url}Tracking/GetStatusOrder?track=$idTracking");
      final _data = jsonDecode(_response.body);
      print(_data);
      if(_data['Data'].length < 1){
        var _result = {"status": false,"message":"Tracking Id not found"};
        return _result;
      }
      else{
        print("Cek isi ${_data['Data'].length}");
        var _listStatusOrder = [];
        for(var i=0; i<_data['Data'].length; i++){
          _listStatusOrder.add(_data["Data"][i]);
        }
        var _result = {"status": true,"listStatusOrder":_listStatusOrder};
        return _result;
      }
    }
    catch(e){
      var _result = {"status": false,"message":e.toString()};
      return _result;
    }
  }

  Future<Map> getTrackingByStatusOrderAll(String statusOrder,String branchId,String startDate,String endDate,String idTracking) async{
    Client _client = Client();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _userDLC = _preferences.getString("userDLC");
    try{
      final _response = await _client.post(
        "${BaseUrl.url}Tracking/GetTrackingCount",
        body: {
          "SZSEARCH": "",
          "SZFROW": "1",
          "SZLROW": "25",
          "SZSTRORDER": "",
          "SZSTATUS": statusOrder,
          "SZBRANCHID": branchId,
          "SZDEALER": _userDLC,
          "SZORDERDATE_START1": startDate,
          "SZORDERDATE_END1": endDate,
          "SZTRACKINGDATE": idTracking
        },
      );
      final _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        var _listDataTrackingCount = [];
        for(var i=0; i<_data['Data']['SZRECORDS'].length;i++){
          _listDataTrackingCount.add(_data['Data']['SZRECORDS'][i]);
        }
        var _result = {"status":true,"listDataTrackingCount":_listDataTrackingCount};
        return _result;
      }
      else{
        var _result = {"status": false,"message":_data['Data']['Message']};
        return _result;
      }
    }
    catch(e){
      var _result = {"status": false,"message":e.toString()};
      return _result;
    }
  }

  Future<Map> getTrackingByExceptStatusOrderAll(String statusOrder,String branchId,String startDate,String endDate,String idTracking) async{
    Client _client = Client();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _userDLC = _preferences.getString("userDLC");

    try{
      final _response = await _client.post(
          "${BaseUrl.url}Tracking/GetSummaryTracking",
          body: {
            "SZSEARCH": "ANDROID",
            "SZFROW": "1",
            "SZLROW": "2000",
            "SZSTRORDER": "7 desc",
            "SZSTATUS": statusOrder,
            "SZBRANCHID": branchId,
            "SZDEALER": _userDLC,
            "SZORDERDATE_START1": startDate,
            "SZORDERDATE_END1": endDate,
            "SZTRACKINGDATE": idTracking
          }
      );
      final _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        var _listDataTrackingCount = [];
        print(_data['Data']);
        for(var i=0; i<_data['Data']['SZRECORDS'].length;i++){
          _listDataTrackingCount.add(_data['Data']['SZRECORDS'][i]);
        }
        var _result = {"status":true,"listDataTrackingByCategory":_listDataTrackingCount};
        return _result;
      }
      else{
        var _result = {"status": false,"message":_data['Data']['Message']};
        return _result;
      }
    }
    catch(e){
      var _result = {"status": false,"message":e.toString()};
      return _result;
    }
  }

  Future<Map> getDetailTrackingOrder(String branchId, String numberApplication) async{
    Client _client = Client();
    try{
      final _response = await _client.get("${BaseUrl.url}Tracking/GetDetailOrder?brid=$branchId&applno=$numberApplication");
      if(_response.statusCode == 200){
        final _data = jsonDecode(_response.body);
        var _dataDetailTrackingOrder = _data['Data'][0];
        var _result = {"status":true,"dataDetailTrackingOrder":_dataDetailTrackingOrder};
        return _result;
      }
      else{
        final _data = jsonDecode(_response.body);
        var _result = {"status":false,"message":_data['Message']};
        return _result;
      }
    }
    catch(e){
      print(e.toString());
      var _result = {"status":false,"message":e.toString()};
      return _result;
    }
  }

  Future<Map> getSubmitOrderList() async{
//    var _dataDummy = [
//      {
//        "OrderID": 7,
//        "DlcCode": "000003",
//        "DLC_Name": null,
//        "BranchID": "0000",
//        "Orderdate": "2020-02-28T10:53:05.323",
//        "OrderNo": "yjgj",
//        "KTP_No": "yyyy",
//        "FirstName": "uiii",
//        "LastName": "yyy",
//        "MidName": "uiii",
//        "BirthPlace": "uiii",
//        "BirthDate": "2020-01-01T00:00:00",
//        "MaritalStatus": "yy",
//        "KTP_Address": "yy",
//        "KTP_RT": "09",
//        "KTP_RW": "09",
//        "KTP_Kel_ID": null,
//        "KTP_Kelurahan": "09",
//        "KTP_Kec_ID": null,
//        "KTP_Kecamatan": "09",
//        "KTP_KabKot_ID": null,
//        "KTP_KabKota": "09",
//        "KTP_Prov_ID": null,
//        "KTP_Provinsi": "09",
//        "KTP_ZipCode": "09",
//        "IsSameLifeAddress": false,
//        "Address": "09",
//        "RT": "09",
//        "RW": "09",
//        "Kel_ID": null,
//        "Kelurahan": "09",
//        "Kec_ID": null,
//        "Kecamatan": "09",
//        "KabKot_ID": null,
//        "Kab_Kota": "09",
//        "Prov_ID": null,
//        "Provinsi": "09",
//        "ZipCode": "09",
//        "Survey_Address": "09",
//        "Phone_No": "09",
//        "Spouse_FirstName": "09",
//        "Spouse_LastName": "09",
//        "Maiden_FirstName": "09",
//        "Maiden_LastName": "09",
//        "Jenis_Kendaraan_ID": null,
//        "Jenis_Kendaraan": "09",
//        "Merk_Kendaraan_ID": null,
//        "Merk_Kendaraan": "09",
//        "Type_Kendaraan_ID": null,
//        "Type_kendaraan": "09",
//        "Model_Kendaraan_ID": null,
//        "Model_Kendaraan": "09",
//        "Tahun_Kendaraan": "09",
//        "OTR": 38.1,
//        "Tenor": 9,
//        "Gross_DP": 38.1,
//        "NET_DP": 38.1,
//        "Installment": 38.1,
//        "SurveyDate": "2020-02-01T09:00:00",
//        "DLC_Note": "09",
//        "HP_No": "09",
//        "Jenis_Angsuran": "09",
//        "LeaseType": "09",
//        "Jenis_Pembiayaan": 9,
//        "BPKB_Name": "09",
//        "Eff_Rate": 51,
//        "Flat_rate": 51,
//        "Pekerjaan": "09",
//        "Purpose": "09",
//        "NoAplikasiUnit": null,
//        "NoAplikasiPayung": null,
//        "CreateUserID": "eggy@ad1gate.com",
//        "NIK_Surveyor": "09",
//        "SurveyorName": "09",
//        "NIK_Marketing": "09",
//        "MarketingName": "09",
//        "Langtitude": "09",
//        "Longtitude": "09",
//        "JenisSurvey": 1,
//        "Gender": "09",
//        "Status": "NSVY",
//        "Remark": "Survey",
//        "Tgl_Verifikasi": null,
//        "Tgl_Survey": null,
//        "Tgl_Approval": null,
//        "Tgl_Reject": null,
//        "Tgl_P3K": null,
//        "Tgl_PO": null,
//        "Tgl_Invoice": null,
//        "Tgl_Cancel_Order": null,
//        "Tgl_Cancel_PO": null,
//        "Tgl_Tagihan": null,
//        "Tgl_PPD": null,
//        "Tgl_Cair": null
//      },
//      {
//        "OrderID": 11,
//        "DlcCode": "000003",
//        "DLC_Name": null,
//        "BranchID": "0321",
//        "Orderdate": "2020-02-28T13:57:45.06",
//        "OrderNo": "987654321",
//        "KTP_No": "0987654321",
//        "FirstName": null,
//        "LastName": "Abdul Ahmad",
//        "MidName": null,
//        "BirthPlace": "Jakarta",
//        "BirthDate": "1990-01-01T00:00:00",
//        "MaritalStatus": "01",
//        "KTP_Address": "Bandung",
//        "KTP_RT": "001",
//        "KTP_RW": "003",
//        "KTP_Kel_ID": null,
//        "KTP_Kelurahan": "01234",
//        "KTP_Kec_ID": null,
//        "KTP_Kecamatan": "01234",
//        "KTP_KabKot_ID": null,
//        "KTP_KabKota": "01234",
//        "KTP_Prov_ID": null,
//        "KTP_Provinsi": "00",
//        "KTP_ZipCode": "012356",
//        "IsSameLifeAddress": true,
//        "Address": null,
//        "RT": "001",
//        "RW": "001",
//        "Kel_ID": null,
//        "Kelurahan": "01234",
//        "Kec_ID": null,
//        "Kecamatan": "01234",
//        "KabKot_ID": null,
//        "Kab_Kota": "01234",
//        "Prov_ID": null,
//        "Provinsi": "00",
//        "ZipCode": "012345",
//        "Survey_Address": "Bandung",
//        "Phone_No": "0853465746",
//        "Spouse_FirstName": null,
//        "Spouse_LastName": null,
//        "Maiden_FirstName": null,
//        "Maiden_LastName": null,
//        "Jenis_Kendaraan_ID": null,
//        "Jenis_Kendaraan": "001",
//        "Merk_Kendaraan_ID": null,
//        "Merk_Kendaraan": "BEBEK",
//        "Type_Kendaraan_ID": null,
//        "Type_kendaraan": "Z oint",
//        "Model_Kendaraan_ID": null,
//        "Model_Kendaraan": "BEAT",
//        "Tahun_Kendaraan": "2019",
//        "OTR": 10000000.0,
//        "Tenor": 20,
//        "Gross_DP": 1000000.0,
//        "NET_DP": 2000000.0,
//        "Installment": 100000.0,
//        "SurveyDate": "2020-04-09T16:00:30",
//        "DLC_Note": null,
//        "HP_No": "0836457383",
//        "Jenis_Angsuran": "ABBD",
//        "LeaseType": null,
//        "Jenis_Pembiayaan": 2,
//        "BPKB_Name": "ABDUL",
//        "Eff_Rate": 0,
//        "Flat_rate": 0,
//        "Pekerjaan": "SWASTA",
//        "Purpose": null,
//        "NoAplikasiUnit": null,
//        "NoAplikasiPayung": null,
//        "CreateUserID": "eggy@ad1gate.com",
//        "NIK_Surveyor": "10987650",
//        "SurveyorName": "RANDI",
//        "NIK_Marketing": "18597490",
//        "MarketingName": "NURUL",
//        "Langtitude": "ABD",
//        "Longtitude": "ABD",
//        "JenisSurvey": 4,
//        "Gender": "1",
//        "Status": "NVer",
//        "Remark": "Verifikasi",
//        "Tgl_Verifikasi": null,
//        "Tgl_Survey": null,
//        "Tgl_Approval": null,
//        "Tgl_Reject": null,
//        "Tgl_P3K": null,
//        "Tgl_PO": null,
//        "Tgl_Invoice": null,
//        "Tgl_Cancel_Order": null,
//        "Tgl_Cancel_PO": null,
//        "Tgl_Tagihan": null,
//        "Tgl_PPD": null,
//        "Tgl_Cair": null
//      }
//    ];
//    var _result = {"status":true,"listSubmitOrder":_dataDummy};
//    return _result;
    Client _client = Client();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _userDLC = _preferences.getString("userDLC");
    String _userID = _preferences.getString("email");
    try{
      final _response = await _client.post("${BaseUrl.url}/Submit/GetListSubmitOrder",
          body: {"UserID": _userID,"CodeDLC":_userDLC,"Search":""}
      );
      if(_response.statusCode == 200){
        final _data = jsonDecode(_response.body);
        List _dataSubmitOrder = [];
        for(var i=0; i < _data['Data'].length; i++){
          _dataSubmitOrder.add(_data['Data'][i]);
        }
        var _result = {"status":true,"listSubmitOrder":_dataSubmitOrder};
        return _result;
      }
      else{
        var _result = {"status":false,"message":"Failed get data"};
        return _result;
      }
    }
    catch(e){
      print(e.toString());
      var _result = {"status":false,"message":"Failed get data"};
      return _result;
    }
  }

  Future<Map> getSubmitOrderListBySearch(String query) async{
    Client _client = Client();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _userDLC = _preferences.getString("userDLC");
    String _userID = _preferences.getString("email");
    try{
      final _response = await _client.post("${BaseUrl.url}/Submit/GetListSubmitOrder",
          body: {"UserID": _userID,"CodeDLC":_userDLC,"Search":query}
      );
      if(_response.statusCode == 200){
        final _data = jsonDecode(_response.body);
        List _dataSubmitOrder = [];
        for(var i=0; i < _data['Data'].length; i++){
          _dataSubmitOrder.add(_data['Data'][i]);
        }
        var _result = {"status":true,"listSubmitOrder":_dataSubmitOrder};
        return _result;
      }
      else{
        var _result = {"status":false,"message":"Failed get data"};
        return _result;
      }
    }
    catch(e){
      print(e.toString());
      var _result = {"status":false,"message":"Failed get data"};
      return _result;
    }
  }

  Future<SubmitOrderModel> getAllListDataSubmitOrder() async{
    Client _client = Client();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _userDLC = _preferences.getString("userDLC");
    String _userID = _preferences.getString("email");
    final _response = await _client.post("${BaseUrl.url}/Submit/GetListSubmitOrder",
        body: {"UserID": _userID,"CodeDLC":_userDLC,"Search":""});
    final _data = jsonDecode(_response.body);
    List _dataSubmitOrder = [];
    for(var i=0; i < _data['Data'].length; i++){
      _dataSubmitOrder.add(_data['Data'][i]);
    }
    return SubmitOrderModel.fromJson(_dataSubmitOrder);
  }

  Future<SubmitOrderModel> getListDataSubmitOrderBySearch(String query) async{
    Client _client = Client();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _userDLC = _preferences.getString("userDLC");
    String _userID = _preferences.getString("email");
    final _response = await _client.post("${BaseUrl.url}/Submit/GetListSubmitOrder",
        body: {"UserID": _userID,"CodeDLC":_userDLC,"Search":query});
    final _data = jsonDecode(_response.body);
    List _dataSubmitOrder = [];
    for(var i=0; i < _data['Data'].length; i++){
      _dataSubmitOrder.add(_data['Data'][i]);
    }
    return SubmitOrderModel.fromJson(_dataSubmitOrder);
  }
}