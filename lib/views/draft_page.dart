import 'package:adira_finance/custom/responsive_screen.dart';
import 'package:adira_finance/db_helper/database_helper.dart';
import 'package:adira_finance/views/home.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import 'submit_page.dart';
import 'detail_data_transaksional.dart';

class DraftPage extends StatefulWidget {
//  final VoidCallback showDialog;
//  const DraftPage({this.showDialog});
//  final String title;
//  final Function onMapTap;
//  DraftPage(this.title, this.onMapTap);
  @override
  _DraftPageState createState() => _DraftPageState();
}

class _DraftPageState extends State<DraftPage> {
  Screen size;
  DataModel _dataModel;
  List<DataModel> _listDraft = [];
  var _newData = [];
  TextEditingController editingController = TextEditingController();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  DbHelper _dbHelper = DbHelper();
  var _getDataProcess = false;
  var _listDraftData = [];

  @override
  void initState() {
    super.initState();
//    prosesData();
    _getDraft();
  }

  onSearchTextChanged(String text) async {
    _newData.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _listDraftData.forEach((dataNasabah) {
      if (dataNasabah['LastName'].toLowerCase().contains(text) || dataNasabah['KTP_No'].toLowerCase().contains(text)){
        setState(() {
          _newData.add(dataNasabah);
        });
      }
    });
  }

  _getDraft() async{
    setState(() {
      _getDataProcess = true;
    });
    var _result = await _dbHelper.getDraft();
    for(var i=0; i < _result.length; i++){
      _listDraftData.add(_result[i]);
    }
    setState(() {
      _getDataProcess = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("Draft",style: TextStyle(fontFamily: "NunitoSans",color: Colors.black)),
        centerTitle: true,
        backgroundColor: myPrimaryColor,
        actions: <Widget>[
          FlatButton(
              onPressed: (){
                showMyDialog();
              },
              child: Text('Kirim Semua',
                  style: TextStyle(fontFamily: "NunitoSans",color: Colors.black)
              )
          )
        ],
      ),
      body: Theme(
        data:ThemeData(primaryColor: Colors.black),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: size.hp(1.5),horizontal: size.wp(2)),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: new InputDecoration(
                    labelText: 'Cari sekarang',
                    labelStyle: TextStyle(fontFamily: "NunitoSans",color: Colors.black),
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                    suffixIcon: Icon(Icons.search)
                ),
                controller: editingController,
                onChanged: onSearchTextChanged,
              ),
              Expanded(
                  child:
                  _getDataProcess
                      ?
                  Center(
                    child: Text("Tidak ada draft tersimpan",style: TextStyle(fontFamily: "NunitoSansSemiBold",fontSize: 18)),
                  )
                      :
                  _newData != null && _newData.length != 0
                      ?
                  ListView.builder(
                    itemBuilder: (context,index){
                      return
                        InkWell(
                          onTap: (){
//                            Navigator.of(context).push(
//                                MaterialPageRoute(builder: (context) =>
//                                    DetailTransaksional(_listDraft[index],1)));
//                            Navigator.of(context).push(
//                              new MaterialPageRoute(builder: (_)=>
//                                  DetailTransaksional(_newData[index],1)),)
//                                .then((val)=>val ?  _showSnackBarFromDraft("Data berhasil masuk"):null);
//                            _navigateAndDisplaySelection(context,_newData[index],1,index);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: size.hp(2)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                    _newData[index]['LastName'],
                                    style: TextStyle(fontFamily: 'NunitoSansSemiBold')
                                ),
                                Text(
                                    _newData[index]['KTP_No'],
                                    style: TextStyle(fontFamily: 'NunitoSans')
                                ),
                              ],
                            ),
                          ),
                        );
                    },
                    itemCount: _newData.length,
                    padding: EdgeInsets.only(top: size.hp(2),left: size.wp(3),right: size.wp(3)),
                  )
                      :
                  ListView.builder(
                    itemBuilder: (context,index){
                      return
                        InkWell(
                          onTap: (){
//                            Navigator.of(context).push(
//                                MaterialPageRoute(builder: (context) =>
//                                    DetailTransaksional(_listDraft[index],1)));
//                            Navigator.of(context).push(
//                              new MaterialPageRoute(builder: (_)=>
//                                  DetailTransaksional(_listDraft[index],1)));
//                            _navigateAndDisplaySelection(context,_listDraft[index],1,index);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: size.hp(2)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                    _listDraftData[index]['LastName'],
                                    style: TextStyle(fontFamily: 'NunitoSansSemiBold')
                                ),
                                Text(
                                    _listDraftData[index]['KTP_No'],
                                    style: TextStyle(fontFamily: 'NunitoSans')
                                )
                              ],
                            ),
                          ),
                        );
                    },
                    itemCount: _listDraftData.length,
                    padding: EdgeInsets.only(top: size.hp(2),left: size.wp(3),right: size.wp(3)),
                  )
              )
            ],
          ),
        ),
      ),
    );
  }

  showMyDialog(){
    showDialog(
        context: context,
        barrierDismissible: false,
        builder:(BuildContext context){
          return AlertDialog(
            title: Text("Anda yakin untuk mengajukan kredit ini?",style: TextStyle(fontFamily: "NunitoSans")),
            actions: <Widget>[
              FlatButton(
                  onPressed: (){
                    Navigator.pop(context);
                    _showSnackBar("Data berhasil dikirim");
                  },
                  child: Text("Lanjutkan",style: TextStyle(fontFamily: "NunitoSansBold",color: yellow))),
              FlatButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  child: Text("Batal",style: TextStyle(fontFamily: "NunitoSansBold",color: yellow))),
            ],
          );
        }
    );
  }

  clearList(){
    setState(() {
      _listDraft.clear();
    });
  }


//  _navigateAndDisplaySelection(BuildContext context, DataModel listDraft,int idHalaman,int index) async {
//    // Navigator.push returns a Future that completes after calling
//    // Navigator.pop on the Selection Screen.
//    final result = await Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => DetailTransaksional(listDraft,idHalaman,index)),
//    );
//    _showSnackBar("Data berhasil dikirim");
//    clearListIndex(result);
//  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
    clearList();
  }

  void _showSnackBarFromDraft(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  prosesData(){
    for(var u in listDraft){
      _dataModel = DataModel(
          u['dealerCabang'],
          u['kendaraan'],
          u['model'],
          u['tipe'],
          u['merk'],
          u['thnProduksi'],
          u['namaBPKB'],
          u['jenisPembiayaan'],
          u['otr'],
          u['tenor'],
          u['ktp'],
          u['namaLengkap'],
          u['namaPanggilan'],
          u['tmptLahir'],
          u['tglLahir'],
          u['statusNikah'],
          u['alamatSurvey'],
          u['kecamatan'],
          u['kelurahan'],
          u['kodePos'],
          u['tlpn'],
          u['hp'],
          u['namaPasangan'],
          u['namaIbu'],
          u['catatan'],
          u['tglSubmit'],
          u['status']
      );
      _listDraft.add(_dataModel);
    }
  }

  var listDraft = [
    {
      "dealerCabang" : "Anugerah Motor",
      "kendaraan":"Motor",
      "model":"Sport",
      "tipe":"CBR 250",
      "merk":"Honda",
      "thnProduksi":"2018",
      "namaBPKB":"Teguh Hendrawan",
      "jenisPembiayaan":"Kredit",
      "otr":"Rp.59,200,000",
      "tenor":"36",
      "ktp":"3571003067760003",
      "namaLengkap":"Lucki Setiawan",
      "namaPanggilan":"Lucki",
      "tmptLahir":"Jakarta",
      "tglLahir":"20 - 12 - 1977",
      "statusNikah":"Kawin",
      "alamatSurvey":"Jl.Seroja no 808 Jakarta Pusat",
      "kecamatan":"Sawah Besar",
      "kelurahan":"Karang Anyar",
      "kodePos":"10740",
      "tlpn":"021-4562389",
      "hp":"0812-5432-6789",
      "namaPasangan":"Lidya Maharani",
      "namaIbu":"Nining",
      "catatan":"Motor minta warna hitam",
    },
    {
      "dealerCabang" : "Anugerah Motor",
      "kendaraan":"Motor",
      "model":"Matic",
      "tipe":"PCX 150",
      "merk":"Honda",
      "thnProduksi":"2018",
      "namaBPKB":"Beni Sanjaya",
      "jenisPembiayaan":"Kredit",
      "otr":"Rp.28,200,000",
      "tenor":"36",
      "ktp":"35710020128760003",
      "namaLengkap":"Teddy Riga",
      "namaPanggilan":"Teddy",
      "tmptLahir":"Jakarta",
      "tglLahir":"20 - 12 - 1987",
      "statusNikah":"Belum Kawin",
      "alamatSurvey":"Jl.Sambungan no 108 Jakarta Pusat",
      "kecamatan":"Sawah Besar",
      "kelurahan":"Karang Anyar",
      "kodePos":"10740",
      "tlpn":"021-4562389",
      "hp":"0812-5432-6789",
      "namaPasangan":"",
      "namaIbu":"Nanik Suharyani",
      "catatan":"Motor minta warna putih",
    }
  ];
}
