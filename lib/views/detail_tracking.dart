import 'dart:io';

import 'package:adira_finance/custom/constan_pop_up_menu.dart';
import 'package:adira_finance/resources/get_data_tracking_api_provider.dart';
import 'package:file_utils/file_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pdf/widgets.dart' as pdfLib;

import '../main.dart';

class DetailTracking extends StatefulWidget {
  final String branchId,nomorApplication;
  const DetailTracking({Key key,this.branchId, this.nomorApplication}) : super(key: key);

  @override
  _DetailTrackingState createState() => _DetailTrackingState();
}

class _DetailTrackingState extends State<DetailTracking> {

  Directory externalDir;
  var path = "No Data";
  String dirLoc = "";
  PermissionStatus permissionStatus;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GetDataTrackingApiProvider _getDataTrackingApiProvider;
  var _dataDetailTrackingOrder;

  checkPermission() async{
    permissionStatus = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
    print(permissionStatus);
    if(permissionStatus == PermissionStatus.denied){
      Map<PermissionGroup, PermissionStatus> permissions =
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      print(permissions.toString());

      if(Platform.isAndroid){
        dirLoc = "/sdcard/Adira PDF";
        try{
          FileUtils.mkdir([dirLoc]);
        }
        catch(e){
          print(e.toString());
        }
      }
      else{
        dirLoc = (await getApplicationDocumentsDirectory()).path;
      }
    }
    else{
      dirLoc = "/sdcard/Adira PDF";
      print("cek dirLoc $dirLoc");
    }
  }

  generatePDF() async {
    final pdfLib.Document pdf = pdfLib.Document();

    final font = await rootBundle.load("fonts/NunitoSans-Regular.ttf");
    final ttf = pdfLib.Font.ttf(font);
    final fontBold = await rootBundle.load("fonts/NunitoSans-Bold.ttf");
    final ttfBold = pdfLib.Font.ttf(fontBold);
    final fontItalic = await rootBundle.load("fonts/NunitoSans-Italic.ttf");
    final ttfItalic = pdfLib.Font.ttf(fontItalic);
    final fontBoldItalic =
    await rootBundle.load("fonts/NunitoSans-Italic.ttf");
    final ttfBoldItalic = pdfLib.Font.ttf(fontBoldItalic);
    final pdfLib.Theme theme = pdfLib.Theme.withFont(
      base: ttf,
      bold: ttfBold,
      italic: ttfItalic,
      boldItalic: ttfBoldItalic,
    );

    pdf.addPage(
        pdfLib.Page(
            pageFormat: PdfPageFormat.a4,
            theme: theme,
            build: (context) {
              return
                pdfLib.Column(
                  crossAxisAlignment: pdfLib.CrossAxisAlignment.start,
                  children: <pdfLib.Widget>[
                    pdfLib.Text('Data Tracking Detail',style: pdfLib.TextStyle(font: ttfBold,fontSize: 18)),
                    pdfLib.SizedBox(height: 16),
                    pdfLib.Row(
                      children:  <pdfLib.Widget>[
                        pdfLib.Expanded(
                            flex: 5,
                            child: pdfLib.Text(
                                "Nomor Aplikasi",
                                style: pdfLib.TextStyle(font: ttf,)
                            )
                        ),
                        pdfLib.Expanded(
                            flex: 1,
                            child: pdfLib.Text(
                                ": ",
                                style: pdfLib.TextStyle(font: ttf,)
                            )
                        ),
                        pdfLib.Expanded(
                            flex: 6,
                            child: pdfLib.Text(
                                "${_dataDetailTrackingOrder['SZAPPLNO']}",
                                style: pdfLib.TextStyle(font: ttf)
                            )
                        ),
                      ]
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                        children:  <pdfLib.Widget>[
                          pdfLib.Expanded(
                              flex: 5,
                              child: pdfLib.Text(
                                  "Branch ID",
                                  style: pdfLib.TextStyle(font: ttf,)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 1,
                              child: pdfLib.Text(
                                  ": ",
                                  style: pdfLib.TextStyle(font: ttf,)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 6,
                              child: pdfLib.Text(
                                  "${_dataDetailTrackingOrder['SZBRID']}",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                        ]
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                        children:  <pdfLib.Widget>[
                          pdfLib.Expanded(
                              flex: 5,
                              child: pdfLib.Text(
                                  "Branch Name",
                                  style: pdfLib.TextStyle(font: ttf,)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 1,
                              child: pdfLib.Text(
                                  ": ",
                                  style: pdfLib.TextStyle(font: ttf,)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 6,
                              child: pdfLib.Text(
                                  "${_dataDetailTrackingOrder['SZBRANCH_NAME']}",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                        ]
                    ),
                    pdfLib.Row(
                        children:  <pdfLib.Widget>[
                          pdfLib.Expanded(
                              flex: 5,
                              child: pdfLib.Text(
                                  "Dealer Name",
                                  style: pdfLib.TextStyle(font: ttf,)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 1,
                              child: pdfLib.Text(
                                  ": ",
                                  style: pdfLib.TextStyle(font: ttf,)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 6,
                              child: pdfLib.Text(
                                  "${_dataDetailTrackingOrder['SZDEALER_NAME']}",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                        ]
                    ),
                    pdfLib.Text('Data Nasabah',style: pdfLib.TextStyle(font: ttfBold,fontSize: 18)),
                    pdfLib.SizedBox(height: 16),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Nomor KTP',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['SZNOKTP'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Nama',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['SZNAME'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Tempat Lahir',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['SZPLACE'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Tanggal Lahir',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['SZBDATE'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Status Pernikahan',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['SZMARITALSTATUS'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Alamat Survey',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['SZADDRESS'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Kecamatan',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['kecamatan'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Kelurahan',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['kelurahan'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Kode Pos',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child:pdfLib.Text(_dataDetailTrackingOrder['kodePos'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Telepon',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child:pdfLib.Text(_dataDetailTrackingOrder['tlpn'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Handphone',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child:pdfLib.Text(_dataDetailTrackingOrder['hp'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Text('Data Pendukung',style: pdfLib.TextStyle(font: ttfBold,fontSize: 18)),
                    pdfLib.SizedBox(height: 16),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Catatan Dealer',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['catatan'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Nama Gadis Ibu Kandung',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['namaIbu'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.Row(
                        children:  <pdfLib.Widget>[
                          pdfLib.Expanded(
                              flex: 5,
                              child: pdfLib.Text(
                                  "Tanggal Order",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 1,
                              child: pdfLib.Text(
                                  ": ",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 6,
                              child: pdfLib.Text(
                                  "${_dataDetailTrackingOrder['SZORDERDATE']}",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                        ]
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                        children:  <pdfLib.Widget>[
                          pdfLib.Expanded(
                              flex: 5,
                              child: pdfLib.Text(
                                  "Nama Pemohon",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 1,
                              child: pdfLib.Text(
                                  ": ",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                          pdfLib.Expanded(
                              flex: 6,
                              child: pdfLib.Text(
                                  "${_dataDetailTrackingOrder['nama_pemohon']}",
                                  style: pdfLib.TextStyle(font: ttf)
                              )
                          ),
                        ]
                    ),
                    pdfLib.SizedBox(height: 8),
//                    pdfLib.Divider(height: 1,color: Colors.black),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Text('Data Kendaraan',style: pdfLib.TextStyle(font: ttfBold,fontSize: 18)),
                    pdfLib.SizedBox(height: 16),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Dealer Cabang',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['dealerCabang'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Jenis Kendaraan',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['kendaraan'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Model',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['model'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Tipe',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['tipe'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Merk',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['merk'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Tahun Produksi',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['thnProduksi'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Nama BPKB',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['namaBPKB'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Text('Data Pengajuan Kredit',style: pdfLib.TextStyle(font: ttfBold,fontSize: 18)),
                    pdfLib.SizedBox(height: 16),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Jenis Pembiayaan',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['jenisPembiayaan'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('OTR',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['otr'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.Row(
                      mainAxisAlignment: pdfLib.MainAxisAlignment.spaceBetween,
                      children: <pdfLib.Widget>[
                        pdfLib.Expanded(child: pdfLib.Text('Tenor',style: pdfLib.TextStyle(font: ttf)),flex: 5,),
                        pdfLib.Expanded(child: pdfLib.Text(':',style: pdfLib.TextStyle(font: ttf)),flex: 1,),
                        pdfLib.Expanded(child: pdfLib.Text(_dataDetailTrackingOrder['tenor'],style: pdfLib.TextStyle(font: ttf)),flex: 6,)
                      ],
                    ),
                    pdfLib.SizedBox(height: 8),
                    pdfLib.SizedBox(height: 8),
                  ]
                );
            })
    );

//    pdf.addPage(
//        pdfLib.MultiPage(
//            theme: theme,
//            build: (context) => [
//              pdfLib.Table.fromTextArray(
//                  context: context,
//                  data: <List<String>>[
//                    [
//                      'asdasasd',
//                      'asdsadsadsad',
//                      'asdasdasdasdasdasdass',
//                      'asd',
//                      'asdasdasd'
//                    ]
//                  ]
//              ),
//            ]
//        )
//    );


    var now = new DateTime.now();
    var formatter = new DateFormat('ddMMyyyyHHmmss');
    String formatted = formatter.format(now);
    print(formatted);
    setState(() {
      path = "$dirLoc/$formatted\.pdf";
    });
    print(path);
    final File file = File(path);
    await file.writeAsBytes(pdf.save());
    _showSnackBar("File save in Internal Storage/Adira PDF");
  }

  @override
  void initState() {
    super.initState();
    checkPermission();
    _getDataTrackingApiProvider = GetDataTrackingApiProvider();
    _getDetailTrackingOrder();
  }

  _getDetailTrackingOrder() async{
    var _result = await _getDataTrackingApiProvider.getDetailTrackingOrder(widget.branchId, widget.nomorApplication);
    if(_result['status']){
      setState(() {
        _dataDetailTrackingOrder = _result['dataDetailTrackingOrder'];
      });
    }
    else{
      print(_result['message']);
    }
  }

    @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Detail Tracking",style:TextStyle(fontFamily: "NunitoSans",color: Colors.black)),
        centerTitle: true,
        backgroundColor: myPrimaryColor,
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: choiceAction,
            itemBuilder: (BuildContext context){
              return Constants.choices.map((String choice){
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          )
        ],
      ),
//      body: Padding(
//        padding: const EdgeInsets.all(8.0),
//        child: ListView(
//          children: <Widget>[
//            Text('Data Submit Order',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
//            Row(
//                children:  <Widget>[
//                  Expanded(
//                      flex: 4,
//                      child: Text(
//                          "Nomor Aplikasi",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                  Expanded(
//                      flex: 0,
//                      child: Text(
//                          ": ",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                  Expanded(
//                      flex: 5,
//                      child: Text(
//                          "${_dataDetailTrackingOrder['SZAPPLNO']}",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                ]
//            ),
//            SizedBox(height: 8),
//            Row(
//                children:  <Widget>[
//                  Expanded(
//                      flex: 4,
//                      child: Text(
//                          "Tanggal Order",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                  Expanded(
//                      flex: 0,
//                      child: Text(
//                          ": ",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                  Expanded(
//                      flex: 5,
//                      child: Text(
//                          "${_dataDetailTrackingOrder['tgl_order']}",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                ]
//            ),
//            SizedBox(height: 8),
//            Row(
//                children:  <Widget>[
//                  Expanded(
//                      flex: 4,
//                      child: Text(
//                          "Nama Pemohon",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                  Expanded(
//                      flex: 0,
//                      child: Text(
//                          ": ",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                  Expanded(
//                      flex: 5,
//                      child: Text(
//                          "${widget.data['nama_pemohon']}",
//                          style: TextStyle(fontFamily: "NunitoSans", fontSize: 16)
//                      )
//                  ),
//                ]
//            ),
//            SizedBox(height: 8),
//            Divider(height: 1,color: Colors.black),
//            SizedBox(height: 8),
//            Text('Data Kendaraan',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
//            SizedBox(height: 8),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Dealer Cabang',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['dealerCabang'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              children: <Widget>[
//                Expanded(child: Text('Jenis Kendaraan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['kendaraan'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Model',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['model'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              children: <Widget>[
//                Expanded(child: Text('Tipe',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['tipe'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Merk',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['merk'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              children: <Widget>[
//                Expanded(child: Text('Tahun Produksi',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['thnProduksi'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Nama BPKB',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['namaBPKB'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            SizedBox(height: 8),
//            Divider(height: 1,color: Colors.black),
//            SizedBox(height: 8),
//            Text('Data Pengajuan Kredit',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
//            SizedBox(height: 8),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Jenis Pembiayaan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['jenisPembiayaan'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('OTR',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['otr'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Tenor',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['tenor'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            SizedBox(height: 8),
//            Divider(height: 1,color: Colors.black),
//            SizedBox(height: 8),
//            Text('Data Nasabah',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
//            SizedBox(height: 8),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Nomor KTP',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['ktp'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Nama Panggilan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['namaPanggilan'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Tempat Lahir',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['tmptLahir'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Tanggal Lahir',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['tglLahir'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Status Perkawinan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['statusNikah'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Alamat Survey',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['alamatSurvey'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Kecamatan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['kecamatan'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Kelurahan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['kelurahan'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Kode Pos',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['kodePos'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Telepon',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['tlpn'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Handphone',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['hp'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            SizedBox(height: 8),
//            Divider(height: 1,color: Colors.black),
//            SizedBox(height: 8),
//            Text('Data Pendukung',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
//            SizedBox(height: 8),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Nama Lengkap Pasangan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['namaPasangan'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Nama Gadis Ibu Kandung',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['namaIbu'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                Expanded(child: Text('Catatan Dealer',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
//                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
//                Expanded(child: Text(widget.data['catatan'],style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
//              ],
//            ),
//          ],
//        ),
//      ),
    );
  }

  void choiceAction(String choice){
    if(choice == Constants.Download){
//      generatePDF();
    }
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState
        .showSnackBar(
        SnackBar(
            content: Text(text),
            behavior: SnackBarBehavior.floating,
            duration: Duration(seconds: 3)
        )
    );
  }
}
