import 'package:adira_finance/custom/responsive_screen.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import 'submit_page.dart';
import 'home.dart';

class DetailTransaksional extends StatefulWidget {
  final DataModel data;
  final int idHalaman;
  const DetailTransaksional(this.data, this.idHalaman);

  @override
  _DetailTransaksionalState createState() => _DetailTransaksionalState();
}

class _DetailTransaksionalState extends State<DetailTransaksional> {

  Screen size;

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail data",style: TextStyle(fontFamily: "NunitoSans",color: Colors.black)),
        centerTitle: true,
        backgroundColor: myPrimaryColor,
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: (){Navigator.pop(context,false);}),
        actions: <Widget>[
          widget.idHalaman == 1
              ?
          FlatButton(
              onPressed: (){
                _showMydialog();
                },
              child: Text('Kirim',style: TextStyle(fontFamily: "NunitoSans",color: Colors.black)))
              :
          SizedBox(height: 0.0,width: 0.0)
        ],
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: size.wp(3)),
        child: ListView(
          padding: EdgeInsets.only(top: size.hp(2),bottom: size.hp(2)),
          children: <Widget>[
            Text('Data Kendaraan',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
            SizedBox(height: size.hp(2)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Dealer Cabang',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.dealerCabang,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(child: Text('Jenis Kendaraan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.kendaraan,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Model',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.model,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(child: Text('Tipe',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.tipe,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Merk',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.merk,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(child: Text('Tahun Produksi',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.thnProduksi,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Nama BPKB',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.namaBPKB,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            SizedBox(height: size.hp(2)),
            Divider(height: 1,color: Colors.black),
            SizedBox(height: size.hp(2)),
            Text('Data Pengajuan Kredit',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
            SizedBox(height: size.hp(2)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Jenis Pembiayaan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.jenisPembiayaan,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('OTR',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.otr,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Tenor',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.tenor,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            SizedBox(height: size.hp(2)),
            Divider(height: 1,color: Colors.black),
            SizedBox(height: size.hp(2)),
            Text('Data Nasabah',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
            SizedBox(height: size.hp(2)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Nomor KTP',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.ktp,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Nama Lengkap',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.namaLengkap,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Nama Panggilan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.namaPanggilan,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Tempat Lahir',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.tmptLahir,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Tanggal Lahir',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.tglLahir,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Status Perkawinan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.statusNikah,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Alamat Survey',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.alamatSurvey,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Kecamatan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.kecamatan,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Kelurahan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.kelurahan,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Kode Pos',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.kodePos,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Telepon',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.tlpn,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Handphone',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.hp,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            SizedBox(height: size.hp(2)),
            Divider(height: 1,color: Colors.black),
            SizedBox(height: size.hp(2)),
            Text('Data Pendukung',style: TextStyle(fontFamily: 'NunitoSansSemiBold',fontSize: 18)),
            SizedBox(height: size.hp(2)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Nama Lengkap Pasangan',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.namaPasangan,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Nama Gadis Ibu Kandung',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.namaIbu,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(child: Text('Catatan Dealer',style: TextStyle(fontFamily: 'NunitoSans')),flex: 5,),
                Expanded(child: Text(':',style: TextStyle(fontFamily: 'NunitoSans')),flex: 1,),
                Expanded(child: Text(widget.data.catatan,style: TextStyle(fontFamily: 'NunitoSans')),flex: 6,)
              ],
            ),
          ],
        ),
      ),
    );
  }

  _showMydialog(){
    showDialog(
        context: context,
        barrierDismissible: false,
        builder:(BuildContext context){
          return AlertDialog(
            title: Text("Anda yakin untuk mengajukan kredit ini?",style: TextStyle(fontFamily: "NunitoSans")),
            actions: <Widget>[
              FlatButton(
                  onPressed: (){
                    Navigator.pop(context);
                    goBack();
                  },
                  child: Text("Lanjutkan",style: TextStyle(fontFamily: "NunitoSansBold",color: yellow))),
              FlatButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  child: Text("Batal",style: TextStyle(fontFamily: "NunitoSansBold",color: yellow))),
            ],
          );
        }
    );
  }

  goBack(){
    Navigator.pop(context,true);
  }
}
