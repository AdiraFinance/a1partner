import 'package:adira_finance/custom/responsive_screen.dart';
import 'package:adira_finance/main.dart';
import 'package:adira_finance/model/submit_order_model.dart';
import 'package:adira_finance/resources/get_data_tracking_api_provider.dart';
import 'package:adira_finance/views/form_transaksional_page.dart';
import 'package:flutter/material.dart';

import 'detail_data_transaksional.dart';
import 'dialog_redeem_voucher.dart';

class SubmitPage extends StatefulWidget {
  @override
  _SubmitPageState createState() => _SubmitPageState();
}

class _SubmitPageState extends State<SubmitPage> {

  Screen size;
  TextEditingController editingController = TextEditingController();
  String filter;
  DataModel _dataModel;
  List<DataModel> _listTrans = [];
  var _listSubmitOrderList = [];
  List<DataModel> _newData = [];
  List<ResultSubmitOrder> _dataSUbmitOrder = [];
  GetDataTrackingApiProvider _getDataTrackingApiProvider;
  var _loadData = false;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
//    prosesData();
    _getDataTrackingApiProvider = GetDataTrackingApiProvider();
//    _getDataSubmitOrder();
  _getListDataSubmitOrder();
  }

  _getDataSubmitOrder() async{
    setState(() {
      _loadData = true;
      _listSubmitOrderList.clear();
    });
    var _result = await _getDataTrackingApiProvider.getSubmitOrderList();
    if(_result['status']){
      for(var i=0; i < _result['listSubmitOrder'].length; i++){
        _listSubmitOrderList.add(_result['listSubmitOrder'][i]);
      }
      setState(() {
        _loadData = false;
      });
    }
    else{
      setState(() {
        _loadData = false;
      });
      _showSnackBar(_result['message']);
    }
  }

  _getDataSubmitOrderBySearch(String query) async{
    setState(() {
      _loadData = true;
      _listSubmitOrderList.clear();
    });
    var _result = await _getDataTrackingApiProvider.getSubmitOrderListBySearch(query);
    if(_result['status']){
      for(var i=0; i < _result['listSubmitOrder'].length; i++){
        _listSubmitOrderList.add(_result['listSubmitOrder'][i]);
      }
      setState(() {
        _loadData = false;
      });
    }
    else{
      setState(() {
        _loadData = false;
      });
      _showSnackBar(_result['message']);
    }
  }

  _getListDataSubmitOrder() async{
    setState(() {
      _loadData = true;
      _dataSUbmitOrder.clear();
    });
    try{
      var _result = await _getDataTrackingApiProvider.getAllListDataSubmitOrder();
      for(int i=0; i <_result.listResultSumbitOrder.length; i++){
        _dataSUbmitOrder.add(_result.listResultSumbitOrder[i]);
      }
      setState(() {
        _loadData = false;
      });
    }
    catch(e){
      setState(() {
        _loadData = false;
      });
      _showSnackBar(e.toString());
    }
  }

  _getListDataSUbmitOrderBySearch(String query) async{
    setState(() {
      _loadData = true;
      _dataSUbmitOrder.clear();
    });
    try{
      var _result = await _getDataTrackingApiProvider.getListDataSubmitOrderBySearch(query);
      for(int i=0; i <_result.listResultSumbitOrder.length; i++){
        _dataSUbmitOrder.add(_result.listResultSumbitOrder[i]);
      }
      setState(() {
        _loadData = false;
      });
    }
    catch(e){
      setState(() {
        _loadData = false;
      });
      _showSnackBar(e.toString());
    }
  }

  onSearchTextChanged(String text) async {
    _newData.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _listTrans.forEach((dataNasabah) {
      if (dataNasabah.ktp.toLowerCase().contains(text) || dataNasabah.namaLengkap.toLowerCase().contains(text)){
        setState(() {
          _newData.add(dataNasabah);
        });
      }
    });
  }

  prosesData(){
    for(var u in listDataTransaksional){
      _dataModel = DataModel(
          u['dealerCabang'],
          u['kendaraan'],
          u['model'],
          u['tipe'],
          u['merk'],
          u['thnProduksi'],
          u['namaBPKB'],
          u['jenisPembiayaan'],
          u['otr'],
          u['tenor'],
          u['ktp'],
          u['namaLengkap'],
          u['namaPanggilan'],
          u['tmptLahir'],
          u['tglLahir'],
          u['statusNikah'],
          u['alamatSurvey'],
          u['kecamatan'],
          u['kelurahan'],
          u['kodePos'],
          u['tlpn'],
          u['hp'],
          u['namaPasangan'],
          u['namaIbu'],
          u['catatan'],
          u['tglSubmit'],
          u['status']);
      _listTrans.add(_dataModel);
    }
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState
        .showSnackBar(
        SnackBar(content: new Text(text,style: TextStyle(
            fontFamily: "NunitoSans",color: Colors.white)),
          behavior: SnackBarBehavior.floating,backgroundColor: Colors.black,));
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: Theme(
        data:ThemeData(primaryColor: Colors.black),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: size.hp(1.5),horizontal: size.wp(2)),
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: size.wp(1.5),right: size.wp(1.5)),
                child: TextFormField(
                  decoration: new InputDecoration(
                      labelText: 'Cari sekarang',
                      labelStyle: TextStyle(fontFamily: "NunitoSans",color: Colors.black),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                      suffixIcon: Icon(Icons.search)
                  ),
                  controller: editingController,
                  textInputAction: TextInputAction.search,
                  onFieldSubmitted: (e){
                    _getListDataSUbmitOrderBySearch(e);
                  },
//                  onChanged: onSearchTextChanged,
                ),
              ),
              Expanded(
                child:
                _loadData
                    ?
                Center(child: CircularProgressIndicator())
                    :                 ListView.builder(
                  itemBuilder: (context,index){
                    return
                      InkWell(
                        onTap: (){
//                          Navigator.of(context).push(
//                              MaterialPageRoute(builder: (context) =>
//                                  DetailTransaksional(_listSubmitOrderList[index],0)));
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: size.hp(1)),
                          child: Card(
                            elevation: 5.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(flex:2,child: Image.asset("img/car.webp")),
                                  Expanded(child: SizedBox(width: 6),flex: 0,),
                                  Expanded(
                                    flex: 5,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                            _dataSUbmitOrder[index].lastName,
                                            style: TextStyle(fontFamily: 'NunitoSansBold')
                                        ),
                                        SizedBox(height: 4),
                                        Text(
                                            _dataSUbmitOrder[index].Jenis_Kendaraan,
                                            style: TextStyle(fontFamily: 'NunitoSans')
                                        ),
                                        SizedBox(height: 4),
                                        Text(
                                            _dataSUbmitOrder[index].Type_kendaraan,
                                            style: TextStyle(fontFamily: 'NunitoSans')
                                        ),
                                        SizedBox(height: 4),
                                        Row(
                                          children: <Widget>[
                                            Text("${_dataSUbmitOrder[index].Tenor}",
                                                style: TextStyle(fontFamily: 'NunitoSansBold',color: Color(0xff56cf00))
                                            ),
                                            SizedBox(width: 4),
                                            Text("Bulan",style: TextStyle(fontFamily: "NunitoSansBold",color: Color(0xff56cf00)))
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                      height: 30,
                                      width: MediaQuery.of(context).size.width/5,
                                      margin: EdgeInsets.all(8),
                                      decoration: BoxDecoration(
                                        color: _dataSUbmitOrder[index].Status == "NVER" ? myPrimaryColor : Color(0xff80eb34),
                                        borderRadius: BorderRadius.circular(25),
                                      ),
                                      child: Center(
                                        child: Text(_dataSUbmitOrder[index].Remark),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                  },
                  itemCount: _dataSUbmitOrder.length,
                  padding: EdgeInsets.only(top: size.hp(2),left: size.wp(2),right: size.wp(2)),
                )
//                ListView.builder(
//                  itemBuilder: (context,index){
//                    return
//                      InkWell(
//                        onTap: (){
////                          Navigator.of(context).push(
////                              MaterialPageRoute(builder: (context) =>
////                                  DetailTransaksional(_listSubmitOrderList[index],0)));
//                        },
//                        child: Container(
//                          margin: EdgeInsets.only(bottom: size.hp(1)),
//                          child: Card(
//                            elevation: 5.0,
//                            shape: RoundedRectangleBorder(
//                              borderRadius: BorderRadius.circular(8.0),
//                            ),
//                            child: Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: Row(
//                                children: <Widget>[
//                                  Expanded(flex:2,child: Image.asset("img/car.webp")),
//                                  Expanded(child: SizedBox(width: 6),flex: 0,),
//                                  Expanded(
//                                    flex: 5,
//                                    child: Column(
//                                      crossAxisAlignment: CrossAxisAlignment.start,
//                                      children: <Widget>[
//                                        Text(
//                                            _listSubmitOrderList[index]['LastName'],
//                                            style: TextStyle(fontFamily: 'NunitoSansBold')
//                                        ),
//                                        SizedBox(height: 4),
//                                        Text(
//                                            _listSubmitOrderList[index]['Jenis_Kendaraan'],
//                                            style: TextStyle(fontFamily: 'NunitoSans')
//                                        ),
//                                        SizedBox(height: 4),
//                                        Text(
//                                            _listSubmitOrderList[index]['Type_kendaraan'],
//                                            style: TextStyle(fontFamily: 'NunitoSans')
//                                        ),
//                                        SizedBox(height: 4),
//                                        Row(
//                                          children: <Widget>[
//                                            Text("${_listSubmitOrderList[index]['Tenor']}",
//                                                style: TextStyle(fontFamily: 'NunitoSansBold',color: Color(0xff56cf00))
//                                            ),
//                                            SizedBox(width: 4),
//                                            Text("Bulan",style: TextStyle(fontFamily: "NunitoSansBold",color: Color(0xff56cf00)))
//                                          ],
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 3,
//                                    child: Container(
//                                      height: 30,
//                                      width: MediaQuery.of(context).size.width/5,
//                                      margin: EdgeInsets.all(8),
//                                      decoration: BoxDecoration(
//                                        color: _listSubmitOrderList[index]['Status'] == "NVER" ? myPrimaryColor : Color(0xff80eb34),
//                                        borderRadius: BorderRadius.circular(25),
//                                      ),
//                                      child: Center(
//                                        child: Text(_listSubmitOrderList[index]['Remark']),
//                                      ),
//                                    ),
//                                  )
//                                ],
//                              ),
//                            ),
//                          ),
//                        ),
//                      );
//                  },
//                  itemCount: _listSubmitOrderList.length,
//                  padding: EdgeInsets.only(top: size.hp(2),left: size.wp(2),right: size.wp(2)),
//                )

//                  child: _newData != null && _newData.length != 0
//                      ?
//                  ListView.builder(
//                    itemBuilder: (context,index){
//                      return
//                        InkWell(
//                          onTap: (){
//                            Navigator.of(context).push(
//                                MaterialPageRoute(builder: (context) =>
//                                    DetailTransaksional(_newData[index],0)));
//                          },
//                          child: Container(
//                            margin: EdgeInsets.only(bottom: size.hp(1)),
//                            child: Card(
//                              elevation: 5.0,
//                              shape: RoundedRectangleBorder(
//                                borderRadius: BorderRadius.circular(8.0),
//                              ),
//                              child: Padding(
//                                padding: const EdgeInsets.all(8.0),
//                                child: Row(
//                                  children: <Widget>[
//                                    Expanded(flex:2,child: Image.asset("img/car.webp")),
//                                    Expanded(child: SizedBox(width: 6),flex: 0,),
//                                    Expanded(
//                                      flex: 5,
//                                      child: Column(
//                                        crossAxisAlignment: CrossAxisAlignment.start,
//                                        children: <Widget>[
//                                          Text("${_newData[index].tglSubmit}, 13:04",style: TextStyle(fontFamily: "NunitoSans")),
//                                          SizedBox(height: 4),
//                                          Text(
//                                              _newData[index].namaLengkap,
//                                              style: TextStyle(fontFamily: 'NunitoSansBold')
//                                          ),
//                                          SizedBox(height: 4),
//                                          Text(
//                                              _newData[index].dealerCabang,
//                                              style: TextStyle(fontFamily: 'NunitoSans')
//                                          ),
//                                          SizedBox(height: 4),
//                                          Text(
//                                              _newData[index].model,
//                                              style: TextStyle(fontFamily: 'NunitoSans')
//                                          ),
//                                          SizedBox(height: 4),
//                                          Row(
//                                            children: <Widget>[
//                                              Text(_newData[index].tenor,
//                                                  style: TextStyle(fontFamily: 'NunitoSansBold',color: Color(0xff56cf00))
//                                              ),
//                                              SizedBox(width: 4),
//                                              Text("Bulan",style: TextStyle(fontFamily: "NunitoSansBold",color: Color(0xff56cf00)))
//                                            ],
//                                          ),
//                                        ],
//                                      ),
//                                    ),
//                                    Expanded(
//                                      flex: 3,
//                                      child: Container(
//                                        height: 30,
//                                        width: MediaQuery.of(context).size.width/5,
//                                        margin: EdgeInsets.all(8),
//                                        decoration: BoxDecoration(
//                                          color: _newData[index].status == "Terkirim" ? myPrimaryColor : Color(0xff80eb34),
//                                          borderRadius: BorderRadius.circular(25),
//                                        ),
//                                        child: Center(
//                                          child: Text(_newData[index].status),
//                                        ),
//                                      ),
//                                    )
//                                  ],
//                                ),
//                              ),
//                            ),
//                          ),
//                        );
//                    },
//                    itemCount: _newData.length,
//                    padding: EdgeInsets.only(top: size.hp(2),left: size.wp(2),right: size.wp(2)),
//                  )
//                      :
//                  ListView.builder(
//                    itemBuilder: (context,index){
//                      return
//                        InkWell(
//                          onTap: (){
//                            Navigator.of(context).push(
//                                MaterialPageRoute(builder: (context) =>
//                                    DetailTransaksional(_listTrans[index],0)));
//                          },
//                          child: Container(
//                            margin: EdgeInsets.only(bottom: size.hp(1)),
//                            child: Card(
//                              shape: RoundedRectangleBorder(
//                                borderRadius: BorderRadius.circular(8.0),
//                              ),
//                              elevation: 5.0,
//                              child: Padding(
//                                padding: const EdgeInsets.all(8.0),
//                                child: Row(
//                                  children: <Widget>[
//                                    Expanded(flex:2,child: Image.asset("img/car.webp")),
//                                    Expanded(child: SizedBox(width: 6),flex: 0,),
//                                    Expanded(
//                                      flex: 5,
//                                      child: Column(
//                                        crossAxisAlignment: CrossAxisAlignment.start,
//                                        children: <Widget>[
//                                          Text("${_listTrans[index].tglSubmit}, 13:04",style: TextStyle(fontFamily: "NunitoSans")),
//                                          SizedBox(height: 4),
//                                          Text(
//                                              _listTrans[index].namaLengkap,
//                                              style: TextStyle(fontFamily: 'NunitoSansBold')
//                                          ),
//                                          SizedBox(height: 4),
//                                          Text(
//                                              _listTrans[index].dealerCabang,
//                                              style: TextStyle(fontFamily: 'NunitoSans')
//                                          ),
//                                          SizedBox(height: 4),
//                                          Text(
//                                              _listTrans[index].model,
//                                              style: TextStyle(fontFamily: 'NunitoSans')
//                                          ),
//                                          SizedBox(height: 4),
//                                          Row(
//                                            children: <Widget>[
//                                              Text(_listTrans[index].tenor,
//                                                  style: TextStyle(fontFamily: 'NunitoSansBold',color: Color(0xff56cf00))
//                                              ),
//                                              SizedBox(width: 4),
//                                              Text("Bulan",style: TextStyle(fontFamily: "NunitoSansBold",color: Color(0xff56cf00)))
//                                            ],
//                                          ),
//                                        ],
//                                      ),
//                                    ),
//                                    Expanded(
//                                      flex: 3,
//                                      child: Container(
//                                        height: 30,
//                                        width: MediaQuery.of(context).size.width/5,
//                                        margin: EdgeInsets.all(8),
//                                        decoration: BoxDecoration(
//                                          color: _listTrans[index].status == "Terkirim" ? myPrimaryColor : Color(0xff80eb34),
//                                          borderRadius: BorderRadius.circular(25),
//                                        ),
//                                        child: Center(
//                                          child: Text(_listTrans[index].status),
//                                        ),
//                                      ),
//                                    )
//                                  ],
//                                ),
//                              ),
//                            ),
//                          ),
//                        );
//                    },
//                    itemCount: _listTrans.length,
//                    padding: EdgeInsets.only(top: size.hp(2),left: size.wp(1.6),right: size.wp(1.6)),
//                  )
              )
            ],
          ),
        ),
      ),
    );
  }

  var listDataTransaksional = [
    {
      "dealerCabang" : "Yanuar Motor",
      "kendaraan":"Motor",
      "model":"Trail",
      "tipe":"KLX 250 cc",
      "merk":"Kawasaki",
      "thnProduksi":"2017",
      "namaBPKB":"Joko Susanto",
      "jenisPembiayaan":"Kredit",
      "otr":"Rp.59,200,000",
      "tenor":"36",
      "ktp":"357100288760003",
      "namaLengkap":"Bambang Saputro",
      "namaPanggilan":"Bambang",
      "tmptLahir":"Jakarta",
      "tglLahir":"20 - 12 - 1977",
      "statusNikah":"Kawin",
      "alamatSurvey":"Jl. Cemara Indah no 707 Jakarta Pusat",
      "kecamatan":"Sawah Besar",
      "kelurahan":"Karang Anyar",
      "kodePos":"10740",
      "tlpn":"021-95032109",
      "hp":"0812-3456-7890",
      "namaPasangan":"Ayu Lidya Sari",
      "namaIbu":"Linda Nina",
      "catatan":"Motor minta warna hitam",
      "tglSubmit":"2 Jan 2020",
      "status":"Terproses"
    },
    {
      "dealerCabang" : "Sukses Motor",
      "kendaraan":"Mobil",
      "model":"Sedan",
      "tipe":"M4 Coupe",
      "merk":"BMW",
      "thnProduksi":"2019",
      "namaBPKB":"Andi Riyadi",
      "jenisPembiayaan":"Kredit",
      "otr":"Rp.2,030,000,000",
      "tenor":"36",
      "ktp":"357100455760009",
      "namaLengkap":"Ganda Lius",
      "namaPanggilan":"Ganda",
      "tmptLahir":"Surabaya",
      "tglLahir":"2 - 10 - 1987",
      "statusNikah":"Kawin",
      "alamatSurvey":"Jl. Kutilang Indah no 107 Jakarta Barat",
      "kecamatan":"Cengkareng",
      "kelurahan":"Cengkareng Barat",
      "kodePos":"11730",
      "tlpn":"021-34577991",
      "hp":"0815-6789-0123",
      "namaPasangan":"Dewi Pertiwi",
      "namaIbu":"Andini Luis Anastasya",
      "catatan":"Mobil minta warna merah",
      "tglSubmit":"30 Jan 2020",
      "status":"Terkirim"
    },
    {
      "dealerCabang" : "Jaya Abadi Motor",
      "kendaraan":"Motor",
      "model":"Sport",
      "tipe":"R25",
      "merk":"Yamaha",
      "thnProduksi":"2018",
      "namaBPKB":"Risky Abdul Latif",
      "jenisPembiayaan":"Kredit",
      "otr":"Rp.59,780,000",
      "tenor":"24",
      "ktp":"3571002807900005",
      "namaLengkap":"Harun Prasetyo",
      "namaPanggilan":"Pras",
      "tmptLahir":"Jakarta",
      "tglLahir":"20 - 12 - 1990",
      "statusNikah":"Belum Kawin",
      "alamatSurvey":"Jl. Persada Permai no 909 Jakarta Selatan",
      "kecamatan":"Cilandak",
      "kelurahan":"Lebak Bulus",
      "kodePos":"12440",
      "tlpn":"021-8769013",
      "hp":"0813-7890-6543",
      "namaPasangan":"",
      "namaIbu":"Sri Ningsih",
      "catatan":"Motor minta yang movistar",
      "tglSubmit":"3 Feb 2020",
      "status":"Terproses"
    }
  ];
}

class DataModel{
  final String dealerCabang;
  final String kendaraan;
  final String model;
  final String tipe;
  final String merk;
  final String thnProduksi;
  final String namaBPKB;
  final String jenisPembiayaan;
  final String otr;
  final String tenor;
  final String ktp;
  final String namaLengkap;
  final String namaPanggilan;
  final String tmptLahir;
  final String tglLahir;
  final String statusNikah;
  final String alamatSurvey;
  final String kecamatan;
  final String kelurahan;
  final String kodePos;
  final String tlpn;
  final String hp;
  final String namaPasangan;
  final String namaIbu;
  final String catatan;
  final String tglSubmit;
  final String status;

  DataModel(this.dealerCabang, this.kendaraan, this.model, this.tipe, this.merk,
      this.thnProduksi, this.namaBPKB, this.jenisPembiayaan, this.otr,
      this.tenor, this.ktp, this.namaLengkap, this.namaPanggilan,
      this.tmptLahir, this.tglLahir, this.statusNikah, this.alamatSurvey,
      this.kecamatan, this.kelurahan, this.kodePos, this.tlpn, this.hp,
      this.namaPasangan, this.namaIbu, this.catatan, this.tglSubmit, this.status);

}
